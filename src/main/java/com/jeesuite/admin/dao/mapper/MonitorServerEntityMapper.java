package com.jeesuite.admin.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.jeesuite.admin.dao.CustomBaseMapper;
import com.jeesuite.admin.dao.entity.MonitorServerEntity;

public interface MonitorServerEntityMapper extends CustomBaseMapper<MonitorServerEntity> {
	
	@Select("SELECT * FROM monitor_servers  where env=#{env} and moudule=#{moudule} limit 1")
	@ResultMap("BaseResultMap")
	MonitorServerEntity findByEnvAndMoudule(@Param("env") String env,@Param("moudule") String moudule);
	
	@Select("SELECT * FROM monitor_servers  where env=#{env}")
	@ResultMap("BaseResultMap")
	List<MonitorServerEntity> findByEnv(String env);
}